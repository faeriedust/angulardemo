﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AngularDemo.Models {
  //avoid sending properties over the wire unless they are needed
  [JsonObject(MemberSerialization.OptIn)]
  public class FooModel {
    #region Constructors
    public FooModel() { }
    public FooModel(string name) {
      this.Name = name;
    }
    #endregion
    #region JsonProperties
    [JsonProperty]
    public string Name { get; set; }
    #endregion
  }
}