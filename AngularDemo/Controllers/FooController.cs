﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using AngularDemo.Models;

namespace AngularDemo.Controllers {
  public class FooController : ApiController {
    [Route("api/Foo/Foo")]
    [HttpGet]
    public FooModel Foo() {
      var model = new FooModel(_RandomString());
      return model;
    }
    #region Helpers
    private string _RandomString() {
      List<string> strings = new List<string>() {
        "lol","rofl","lmao"
      };
      Random random = new Random();
      int index = random.Next(strings.Count);
      return strings[index];
    }
    #endregion
  }
}