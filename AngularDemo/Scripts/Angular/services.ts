﻿declare var angular;
declare var _root: string;

(function (ng) {
    var servicesModule = ng.module('myApp.services', ['ngResource']);
    servicesModule.service('FooService', ['$resource', function ($resource) {
        var _Foos = [];
        this.GetFoos = function () {
            return _Foos;
        }
        this.AddFoo = function () {
            _Foos = _Foos.concat([$resource(_root + "api/Foo/Foo").get()]);
        }
        //start with one in the list
        this.AddFoo();
    }]);
})(angular);