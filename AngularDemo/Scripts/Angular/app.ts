﻿declare var angular;
declare var _root: string;

(function (ng) {
    var myApp = ng.module('myApp', [
        'myApp.controllers',
        'myApp.services'
    ]);
})(angular);

