(function (ng) {
    var myApp = ng.module('myApp', [
        'myApp.controllers',
        'myApp.services'
    ]);
})(angular);
//# sourceMappingURL=app.js.map