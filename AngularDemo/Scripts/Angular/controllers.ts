﻿declare var angular;

(function (ng) {
    var controllersModule = ng.module('myApp.controllers', []);
    controllersModule.controller('FooCtrl', ['$scope', 'FooService', function ($scope, FooService) {
        $scope.foos = FooService.GetFoos();
        $scope.addFoo = function () {
            FooService.AddFoo();
            $scope.foos = FooService.GetFoos();
        }
    }]);
})(angular); 